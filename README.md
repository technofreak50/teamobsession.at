## About

Source for the websites:

- https://www.teamobsession.at
- https://freerunningacademy.at

## Usage

Deployed to [GitLab Pages](https://gitlab.com/team-obsession/teamobsession.at/pages) with configured custom domains.

- Domain owner: @dnsmichi
- DNS admin: @lazyfrosch

## Updates

1. Follow the [development documentation](#development) to make changes and preview them.
2. Create a new Git branch, commit the changes, push them and create a merge request.
3. Verify that all checks passed, and merge the merge request.
4. The GitLab Pages deployment from the `main` branch is triggered automatically, and deploys the new content to the website.

## Development

### Gitpod

Open https://gitpod.io/#https://gitlab.com/team-obsession/teamobsession.at with permissions to the source project on GitLab.com 

When asked by Gitpod, login using GitLab oauth.

### Local

Requires

- NodeJS

Dependencies:

```shell
npm install 
```

Rebuild CSS 

```shell
npx --yes browserslist@latest --update-db 
npm run build-css
```

Run the webserver, starting on http://localhost:3000

```shell
npx --yes serve
```

## Thanks

[Tobias Huch](https://github.com/Tobias-Huch) for creating this website. 

